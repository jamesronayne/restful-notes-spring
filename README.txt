Notes!

cd to this directory build using: 
./gradlew clean build

run using:
java -jar build/libs/notes-0.0.1.jar

localhost:8080 to the main webpage, can create, delete, edit and list notes stored in memory

uses a RESTful api
/createNote - to create notes send object in body
/editNote - to edit existing note send object in body
/listNotes/ - to return a json object of all notes stored
/deleteNote/{id}/ - to delete node with specific id
/getNote/{id}/ - to retrieve json representation of note

note object includes values: id, lastModifiedDate, user, content

Mockito is used for unit testing
Pure.css was used for styling and JQuery just to handle the ajax requests