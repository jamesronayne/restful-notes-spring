package com.notes;

public class Note {

	private long id;
	private String lastChangeDate;
	private String content;
	private String user;
	
	public Note() {
		super();
	}
	
	public Note(long id, String lastChangeDate, String content, String user) {
        this.id = id;
        this.lastChangeDate = lastChangeDate;
        this.content = content;
        this.user = user;
    }
	
	public void setNote(String newNote) {
		this.content = newNote;
	}
	
	public void setUser(String user) {
		this.user = user;
	}
	
	public void setLastChangedDate(String dateTime) {
		this.lastChangeDate = dateTime;
	}

    public long getId() {
        return id;
    }
    
    public String getLastChangeDate() {
        return lastChangeDate;
    }

    public String getContent() {
        return content;
    }
    
    public String getUser() {
        return user;
    }

	public void setLastChangeDate(String lastChangeDate) {
		this.lastChangeDate = lastChangeDate;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
