package com.notes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class NoteService {
    private final AtomicLong counter = new AtomicLong();
    // Key:noteID Value:Note
    private ConcurrentHashMap<Long, Note> notes = new ConcurrentHashMap<Long,Note>();
    
    public NoteService() {}
    
    /**
     * @param user - User name used to create note
     * @param content - Text content of note
     * @return
     */
    public Long createNote(Note note) {
    	
    		String creationDate = new Date().toString();
		long id = counter.incrementAndGet();
		Note newNote = new Note(id,
                creationDate, note.getContent(), note.getContent());
		notes.put(newNote.getId(), newNote);
		
		return newNote.getId();
    }
    
    /**
     * @return Returns ArrayList of all note objects
     */
    public List<Note> findAll() {
    		return new ArrayList<Note>(notes.values());
    }
    
    /**
     * @param user - new user name to be used to save note
     * @param id - id number of note to be edited
     * @param content - new text content to update note with
     */
    public Note editNote(Note note) {
    		note.setLastChangedDate(new Date().toString());
		notes.replace(note.getId(), note);
		return note;
    }
    
    /**
     * @param id - ID of note to be removed
     */
    public void deleteNote(Long id) {
    		notes.remove(id);
    }
    
    /**
     * @param id - ID number of note to be retrieved
     * @return Note object
     */
    public Note getNote(Long id) {
    		return notes.get(id);
    }
    
    public void add(Note note) {
    		if(!exists(note)) notes.put(note.getId(), note);
    }
    
    public boolean exists(Note note) {
    		return notes.containsKey(note.getId());
    }
    
    public boolean exists(Long id) {
		return notes.containsKey(id);
}
}
