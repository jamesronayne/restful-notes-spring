package com.notes;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.notes.exceptions.NotFoundException;

@RestController
public class NoteController {
    private NoteService noteService = new NoteService();

	@RequestMapping(path = "/createNote", method = RequestMethod.POST)
	public ResponseEntity<Note> createNote(@RequestBody Note note) {
		Long id =  noteService.createNote(note);
		String uri = "/getNote/" + id + "/";
		UriComponents uriComponents =
	            UriComponentsBuilder.newInstance().path(uri)
	                .build();
		return ResponseEntity.status(HttpStatus.CREATED).location(uriComponents.toUri()).body(noteService.getNote(id));
		
	}
	
	@RequestMapping(path = "/editNote", method = RequestMethod.PUT)
	public @ResponseBody Note editNote(@RequestBody Note note) throws NotFoundException {
		
		if(!noteService.exists(note.getId())) {
			throw new NotFoundException(0);
		}
		
		return noteService.editNote(note);
	}
	
	@RequestMapping(path = "/deleteNote/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteNote(@PathVariable(value="id") Long id) throws NotFoundException {
		if(!noteService.exists(id)) {
			throw new NotFoundException(0);
		}
		
		noteService.deleteNote(id);
		
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}
	
	@RequestMapping(path = "/getNote/{id}", method = RequestMethod.GET)
	public Note getNote(@PathVariable(value="id") Long id) throws NotFoundException {
		
		if(!noteService.exists(id)) {
			throw new NotFoundException(0);
		} else {
			return noteService.getNote(id);
		}
	}
	
	@RequestMapping(value = "/listNotes", method = RequestMethod.GET)
	public List<Note> getAllNotes() {
		return noteService.findAll();
	}
}