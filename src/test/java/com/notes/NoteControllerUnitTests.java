package com.notes;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class NoteControllerUnitTests {
	
    private MockMvc mockMvc;

    @Mock
    private NoteService noteService;

    @InjectMocks
    private NoteController noteController;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(noteController)
                .build();
    }
    
    @Test
    public void test_get_all_success() throws Exception {
        List<Note> users = Arrays.asList(
                new Note(1,"a","b","c"),
                new Note(2,"d","e","f"));
        when(noteService.findAll()).thenReturn(users);
        mockMvc.perform(get("/listNotes"))
        			.andExpect(status().isOk())
        			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        			.andExpect(jsonPath("$", hasSize(2)))
        			.andExpect(jsonPath("$[0].id", is(1)))
        			.andExpect(jsonPath("$[0].lastChangeDate", is("a")))
        			.andExpect(jsonPath("$[0].content", is("b")))
        			.andExpect(jsonPath("$[0].user", is("c")))
        			.andExpect(jsonPath("$[1].id", is(2)))
        			.andExpect(jsonPath("$[1].lastChangeDate", is("d")))
        			.andExpect(jsonPath("$[1].content", is("e")))
        			.andExpect(jsonPath("$[1].user", is("f")));
        verify(noteService, times(1)).findAll();
        verifyNoMoreInteractions(noteService);
    }

    @Test
    public void test_get_by_id_success() throws Exception {
        Note note = new Note(1, "a", "b", "c");

        when(noteService.getNote((long) 1)).thenReturn(note);
        when(noteService.exists((long) 1)).thenReturn(true);

        mockMvc.perform(get("/getNote/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.lastChangeDate", is("a")))
                .andExpect(jsonPath("$.content", is("b")))
                .andExpect(jsonPath("$.user", is("c")));

        verify(noteService, times(1)).getNote((long) 1);
        verify(noteService, times(1)).exists((long) 1);
        verifyNoMoreInteractions(noteService);
    }
    
    @Test
    public void test_get_by_id_fail_404_not_found() throws Exception {

        when(noteService.getNote((long) 1)).thenReturn(null);

        mockMvc.perform(get("/getNote/{id}", (long) 1))
                .andExpect(status().isNotFound());

        verify(noteService, times(1)).exists((long) 1);
        verifyNoMoreInteractions(noteService);
    }

    @Test
    public void test_create_note_success() throws Exception {
        Note note = new Note(0,"a","b","c");
        
        when(noteService.createNote(note)).thenReturn(note.getId());

        mockMvc.perform(
        		post("/createNote")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(note)))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", containsString("/getNote/0/")));

    }

    @Test
    public void test_update_note_success() throws Exception {
        Note note = new Note(1,"a","b","c");

        when(noteService.exists((long) 1)).thenReturn(true);
        
        mockMvc.perform(
                put("/editNote")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(note)))
                .andExpect(status().isOk());

        verify(noteService, times(1)).exists(note.getId());
    }

    @Test
    public void test_update_note_fail_404_not_found() throws Exception {
        Note note = new Note(1, "a","b","c");

        when(noteService.getNote(note.getId())).thenReturn(null);

        mockMvc.perform(
        			put("/editNote")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(note)))
                .andExpect(status().isNotFound());

        verify(noteService, times(1)).exists(note.getId());
        verifyNoMoreInteractions(noteService);
    }

    @Test
    public void test_delete_note_success() throws Exception {
        Note note = new Note(1,"a","b","c");

        when(noteService.getNote(note.getId())).thenReturn(note);
        when(noteService.exists((long) 1)).thenReturn(true);
        doNothing().when(noteService).deleteNote(note.getId());

        mockMvc.perform(
                delete("/deleteNote/{id}", note.getId()))
                .andExpect(status().isOk());

        verify(noteService, times(1)).deleteNote(note.getId());
        verify(noteService, times(1)).exists((long) 1);
        verifyNoMoreInteractions(noteService);
    }

    @Test
    public void test_delete_note_fail_404_not_found() throws Exception {
        Note note = new Note(33,"a","b","c");

        when(noteService.getNote(note.getId())).thenReturn(null);

        mockMvc.perform(
                delete("/deleteNote/{id}", note.getId()))
                .andExpect(status().isNotFound());

        verify(noteService, times(1)).exists(note.getId());
        verifyNoMoreInteractions(noteService);
    }
    
    /*
     * converts a Java object into JSON representation
     */
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}